package com.example.prueba2nov;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.prueba2nov.persistencia.DbHelper;

public class Registro extends AppCompatActivity {

    Button btnRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        //conexion al boton registrarme con el id btnRegistro
        btnRegistro = findViewById(R.id.btnRegistro);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //instanciacion de la clase de conexion DbHelper
                DbHelper dbHelper = new DbHelper(Registro.this);

                SQLiteDatabase db = dbHelper.getWritableDatabase(); //Crearla = Escribir la base de datos
                // Validación de la base de datos
                if(db != null){
                    Toast.makeText(Registro.this, "BASE DE DATOS CREADA",Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(Registro.this,"ERROR AL CREAR BASE DE DATOS", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}