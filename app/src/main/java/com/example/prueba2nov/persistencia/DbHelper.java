package com.example.prueba2nov.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NOMBRE = "prueba2nov.db";
    public static final String TABLE_USERS = "usuarios";

    public DbHelper(@Nullable Context context) {
        super(context, DATABASE_NOMBRE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_USERS + "("+
                "idusuario INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nombrep TEXT NOT NULL,"+
                "tipop TEXT NOT NULL,"+
                "imgpath TEXT NOT NULL)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL(" DROP TABLE " + TABLE_USERS);
        onCreate(sqLiteDatabase);

    }
}