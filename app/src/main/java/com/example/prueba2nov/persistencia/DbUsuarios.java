package com.example.prueba2nov.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

public class DbUsuarios extends DbHelper{

    Context context;

    // Constructor
    public DbUsuarios(@Nullable Context context) {
        super(context);
        this.context = context;
    }

    public long insertarUsuario(String nombreu, String contraseña){

        long id = 0;

        try{
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("nombre_u", nombreu);
            values.put("password", contraseña);

            id = db.insert(TABLE_USERS, null, values);

        } catch (Exception ex){
            ex.toString();
        }


        return id;

    }
}